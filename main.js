const io = require('socket.io')();
let users = {};
let index = 0;
let history = [];

io.on('connection', (client) => {
    // Assign a username
    // @TODO: Allow user to select their own username
    const username = "Default_" + client.id;

    console.log("[New connection] Username assigned as: " + username);
    users[client.id] = {username: username, client: client};
    client.emit("usernameAssigned", username);
    client.emit("initHistory", history);

    client.broadcast.emit("userConnected", username);
    client.on("disconnect", () => {
        client.broadcast.emit("userDisconnected", users[client.id].username);
    });

    client.on("message", (data, fn) => {
        console.log("[Message] " + data.username + ": " + data.message);
        const timestamp = new Date();
        history.push({username: data.username, message: data.message, timestamp: timestamp.toISOString()});
        client.broadcast.emit("message", {username: data.username, message: data.message, timestamp: timestamp.toISOString()});
        fn(true);
    })
});

const port = 8000;
io.listen(port);
console.log('listening on port ', port);